
class KeyValue
{
public:
    int key;
    int val; 
    friend bool operator== (const KeyValue& k, const KeyValue& k2);
    KeyValue() : key{}, val{}
    {}
    KeyValue(int k, int v) : key{ k }, val{ v }
    {}
    ~KeyValue()
    {}
};
