#include "MyHashTable.h"

MyHashTable::MyHashTable()
{
    std::vector<std::forward_list<KeyValue>> d{ 10 };
    size = 10;
    data = d;
    loadSize = size * 0.7;
    occupiedSlots = 0;
}

int MyHashTable::accessElement(int key)
{
    try
    {
        int hashOfLookupKey = hash_function(key);
        std::forward_list<KeyValue>::iterator it = data[hashOfLookupKey].begin();
        while (it != data[hashOfLookupKey].end())
        {
            if (it->key == key)
            {
                return it->val;
            }
            else
            {
                ++it;
            }
        }
        throw key;
    }
    catch (int n)
    {
        std::cerr << "Error: no value found at key: " << n;
    }
}

void MyHashTable::deleteElement(int key)
{
    try
    {
        int hashOfLookupKey = hash_function(key);
        std::forward_list<KeyValue>::iterator it = data[hashOfLookupKey].begin();
        while (it != data[hashOfLookupKey].end())
        {
            if (it->key == key)
            {
                data[hashOfLookupKey].remove(*it);
                --occupiedSlots;
                return;
            }
            else
            {
                ++it;
            }
        }
        throw key;
    }
    catch (int n)
    {
        std::cerr << "Error: nothing to delete at key: " << n;
    }
}

 void MyHashTable::insertElement(KeyValue newElement)
{
    int arrayIndexAfterHash = hash_function(newElement.key);
    if (!(data[arrayIndexAfterHash].empty()))
    {
        std::forward_list<KeyValue>::iterator it = data[arrayIndexAfterHash].begin();
        while (it != data[arrayIndexAfterHash].end())
        {
            if (it->key == newElement.key)
            {
                it->val = newElement.val;
                return;
            }
            else
            {
                ++it;
            }
        }
    }
    ++occupiedSlots;
    if (occupiedSlots >= loadSize)
    {
        loadSizeReached();
    }
    data[arrayIndexAfterHash].push_front(newElement);
}

 void MyHashTable::loadSizeReached()
 {
     size = size * 2;
     std::vector<std::forward_list<KeyValue>> newContainer{ size };
     loadSize = size * 0.7;
     std::vector<std::forward_list<KeyValue>> temporaryContainer;
     temporaryContainer = data;
     data = newContainer;
     occupiedSlots = 0;
     for (std::forward_list<KeyValue> currentIndex : temporaryContainer)
     {
         std::forward_list<KeyValue>::iterator currentSingleElement = currentIndex.begin();
         while (currentSingleElement != currentIndex.end())
         {
             insertElement(*currentSingleElement);
             ++currentSingleElement;
         }
     }
 }

int MyHashTable::hash_function(int x)
 {
     return x % size;
 }
