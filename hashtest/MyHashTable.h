#pragma once
#include <iostream>
#include <vector>
#include <exception>
#include "KeyValue.h"
#include <forward_list>
class MyHashTable
{
    unsigned int size;
    unsigned int occupiedSlots;
    unsigned int loadSize;
    std::vector <std::forward_list<KeyValue>> data;
public:
    MyHashTable();
    int accessElement(int key);
    void deleteElement(int key);
    void insertElement(KeyValue newElement);
    ~MyHashTable()
    {}
private:
    void loadSizeReached();
    int hash_function(int x);
};

